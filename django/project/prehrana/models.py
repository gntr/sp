from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from datetime import datetime



class Restaurant(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, default="0.0")
    latitude = models.DecimalField(max_digits=9, decimal_places=6, default="0.0")
    price = models.FloatField("student meal price", default=-1)
    rating = models.FloatField("average rating", default=-1)

    vegi = models.BooleanField( default=False)
    delivery = models.BooleanField( default=False)
    sallad = models.BooleanField( default=False)
    pizza = models.BooleanField(default=False)
    lunch = models.BooleanField( default=False)
    fastfood = models.BooleanField( default=False)
    invalid_friendly = models.BooleanField( default=False)

    def __str__(self):
        return self.name


class Menu(models.Model):
    food = models.TextField("menu")
    date = models.DateField()
    restaurant_id = models.ForeignKey(Restaurant, on_delete=models.CASCADE)

    class Meta:
        permissions = (
            ('edit_menu', 'Can edit the menus'),
        )




class Feedback(models.Model):
    rating = models.IntegerField()
    comment = models.TextField()
    restaurant_id = models.ForeignKey(Restaurant)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.user_id)+" "+str(self.rating)+" "+str(self.restaurant_id)


from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver




@receiver(post_delete, sender=Feedback)
def update_rating(sender, instance, **kwargs):
    restaurant = instance.restaurant_id
    feedbacks = Feedback.objects.filter(restaurant_id=restaurant.id)
    restaurant.rating = sum([object.rating for object in feedbacks])/len(feedbacks)
    print([object.rating for object in feedbacks])
    restaurant.save()


@receiver(post_save, sender=Feedback)
def update_rating2(sender, instance, **kwargs):
    restaurant = instance.restaurant_id
    feedbacks = Feedback.objects.filter(restaurant_id=restaurant.id)
    restaurant.rating = sum([object.rating for object in feedbacks])/len(feedbacks)
    print([object.rating for object in feedbacks])
    restaurant.save()

