from django.contrib import admin

from prehrana.models import *


admin.site.register(Feedback)
admin.site.register(Restaurant)
admin.site.register(Menu)

# Register your models here.
