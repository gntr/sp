"""fri_ws URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.views.generic import DetailView
from prehrana.models import Restaurant
from django.contrib.auth.views import login
from . import views

urlpatterns = [
    url(r'^index/$', views.index, name='index'),
    url(r'^search/$', views.search, name='search'),
    url(r'^locals/(?P<restaurant_id>\d+)$', views.locals, name='locals', ),
    url(r'^locals/$', views.locals, name='locals', ),
    url(r'^login/$', views.login_view , name=""),
    url(r'^logout/$', views.logout_user, name=""),

    url(r'^contact/$', views.contact, name='contact'),
    #url(r'^logout/', views.logout_user, name='logout'),
    #url(r'^article/(?P<article_id>[0-9]+)/$', views.article_view, name='article_view'),
    #url(r'^article/(?P<article_id>[0-9]+)/edit/$', views.article_edit, name='article_edit'),
]
