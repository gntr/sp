# Prikaz ponudnikov študentske prehrane #

##Ideja:
Spletna stran uporabniku omogoča iskanje in primerjavo ponudnikov študentske prehrane v Sloveniji. 
Omogoča mu iskanje ponudnikov v bližini (polje za vnos naslova ali avtomatska zaznava koordinat uporabnika) 
ter filtriranje le-teh glede na ponudbo, ceno, in priljubljenost. 
Glavni vir dohodka bi bili morebitni pametno integrirani oglasi za določene ponudnike študentske prehrane. 
Da bi lahko aplikacija dosegla čim večje število uporabnikov, bi zagotovili delovanje na vseh platformah (pc, mobilni telefon, tablica).
Trenutno strani za iskanje BLIŽNJIH ponudnikov ni zato se nam zdi ideja uporabna.

##Opis strani:
Domača stran poleg možnosti iskanja pomuja lestvico najbolj priljubljenih ponudnikov in sponzoriran predlog. 
Stran za iskanje poleg iskanja omogoča prikaz rezultatov v seznamu in na zemljevidu google. 
Klik na elemente seznama ali na markerje na zemljevidu vas popelje na stran za prikaz restavracij. 
Ta omogoča brskanje med restavracijami in prikazuje osnovne podatke o izbrani restavraciji, 
lokacijo na zemljevidu, jedilnik in komentarje uporabnikov. Omogoča tudi enostavno dodajanje komentarjev.
Zadnja je stran s preprostimi kontaktnimi podatki.

##Ciljna publika in naprave:
Ciljna publika so seveda lačni študenti, ki iščejo najbližje/najboljše/najcenejše  ponudnike študentske hrane.
Spletna stran deluje na zadnjih verzijah chroma, firefoxa in opere. Od naprav je bila testirana na PC-ju in mobilnem 
telefonu android, a bi morala delovati tudi na tablicah.

##Težave na Različnih brskalnikih:
Večjih težav ni bilo. Za dokončno zagotovitev delovanja na IE nam je zmanjkalo časa, 
zato načeloma deluje vse razen izmeničnega prikaza komentarjev/jedilnika. Nekaj težav smo imeli z resize funkcijo na 
mobilnih napravah a smo jih odpravili z definicijo vievporta v glavi vsakega od html dokumentov.

##Poseben trud:
S celotno stranjo sem kar zadovoljen, sploh glede na to, da nisem imel skoraj nič predznanja iz spletnih tehnologij.
Največ truda sem vložil v zanesljivo delovanje iskanja skupaj z lastnim gui vmesnikom, ki je po mojem mnenju veliko
primernejši kot bi bil v primeru uporabe privzetih form. Ponosen sem tudi na funkcionalnosi povezane z lokacijami in 
zemljevidi, ki so mi vzele precej časa.  

##Dodatno:
Manjka še možnost ocenjevanja restavracij. Simboličen prikaz ocen je kopiran iz https://css-tricks.com/star-ratings/.
Za lasten prikaz in možnost ocenjevanja je zmanjkalo časa, zato smo sklenili, da to rešimo naknadno, ko bo dovoljena uporaba knjižnic. 
Trenutno uporabljene slike so simbolične in niso moje, zato bodo v končni verziji zamenjane. 

##izdelal: 
Klemen Gantar, FRI

Stran je dostopna na: https://studentska-prehrana.aerobatic.io/


