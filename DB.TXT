CREATE TABLE filter(
   id INT PRIMARY KEY NOT NULL,
   vegi INT,
   delivery INT,
   pizza INT,
   sallad INT
);

CREATE TABLE user(
   id CHAR(30) PRIMARY KEY NOT NULL,
   username CHAR(20) NOT NULL
);

CREATE TABLE restaurant(
   id INT PRIMARY KEY NOT NULL,
   name CHAR(100) NOT NULL,
   address CHAR(100) NOT NULL,
   price REAL,
   rating INT,
   filter_id INT,
   FOREIGN KEY(filter_id) REFERENCES filter(id)
);

CREATE TABLE menu(
   id INT PRIMARY KEY NOT NULL,
   food TEXT NOT NULL,
   date REAL,
   restaurant_id INT,
   FOREIGN KEY(restaurant_id) REFERENCES restaurant(id)
);

CREATE TABLE feedback(
   id INT PRIMARY KEY NOT NULL,
   rating INT,
   comment TEXT, 
   restaurant_id INT,
   user_id CHAR(30),
   FOREIGN KEY(restaurant_id) REFERENCES restaurant(id)
   FOREIGN KEY(user_id) REFERENCES user(id)
);

CREATE TRIGGER update_rating AFTER UPDATE OF rating ON feedback 
  BEGIN
    UPDATE restaurant SET rating = avg(rating) FROM feedback WHERE id = new.restaurant_id;
  END;



