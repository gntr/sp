var filterButtons;
var locationInput;
var locationButton;
var criteria1, criteria2;
var searchError;
var searchForm;
var menu;
var comments;
var menuList;
var commentList;
var searchLocalsInput;

// init for search.html and index.html
function init() {
    // DOM
    filterButtons = document.getElementById("filters").getElementsByClassName("btn");
    locationInput = document.getElementById("locationInput");
    locationButton = document.getElementById("locationButton");
    criteria1 = document.getElementById("criteria1");
    criteria2 = document.getElementById("criteria2");
    searchButton = document.getElementById("searchButton");
    searchError = document.getElementById("searchError");
    searchForm = document.getElementById("searchForm");

    // event listeners
    for (var i = 0; i < filterButtons.length; i++) {
        filterButtons[i].addEventListener('click', function () {
            updateSearchSettings(this, "filters");
        }, false);
    }

    locationInput.addEventListener('focus', function () {
        updateSearchSettings(this, "location");
    }, false);
    locationButton.addEventListener('click', function () {
        updateSearchSettings(this, "location");
        getLocation();
    }, false);

    criteria1.addEventListener('click', function () {
        updateSearchSettings(this, "criteria");
    }, false);
    criteria2.addEventListener('click', function () {
        updateSearchSettings(this, "criteria");
    }, false);

    searchButton.addEventListener('click', function () {
        search();
    }, false);

    // update gui according to url atribute searchid
    var searchId = location.search.split('?filter=')[1];
    if (searchId) {
        for (var i = 0; i < filterButtons.length; i++) {
            if (searchId[i] == "t")
                filterButtons[i].className += " selected";
        }
        if (searchId.split("criteria=")[1][0]== "1") {
            criteria1.className += " selected";
        } else {
            criteria2.className += " selected";
        }
        locationInput.className += " selected";
        locationInput.value = searchId.split("loc=")[1];

        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({"address": locationInput.value}, function (results, status) {
            if (status == 'OK') {
                x = results[0].geometry.location.lat();
                y = results[0].geometry.location.lng();
                updateMap({lat: x, lng: y}, true);

            } else {
                searchError.text = "Iskanje lokacije ni bilo uspešno: status " + status;
            }
        });
    }
} // end function start

//init for locals.html
function initLocals() {
    // DOM
    menu = document.getElementById("menu");
    comments = document.getElementById("comments");
    menuList = document.getElementById("menu-list");
    commentList = document.getElementById("comment-list");
    searchLocalsInput = document.getElementById("searchLocalsInput");

    // event listeners
    menu.addEventListener('click', function () {
        updateSearchSettings(this, "selection");
        switchList(this);
    }, false);
    comments.addEventListener('click', function () {
        updateSearchSettings(this, "selection");
        switchList(this);
    }, false);
    searchLocalsInput.addEventListener('focusout', function () {
        checkInput(this);
    }, false);

    //update map from hidden location paragraph
    locations = document.getElementsByClassName("coordinates");
    coords = locations[0].innerHTML.split(",");
    updateMap( {lat: parseFloat(coords[0]), lng: parseFloat(coords[1])}, true);

}

// locals search verifies if input exists
function checkInput(input) {
    var datalist = document.getElementById('locals').options;
    if (null == datalist.namedItem(input.value)) {
        input.value = "";
    }
}

// switch visibility of menulist and commentlist
function switchList(button) {
    if (button.id == "menu") {
        menuList.style = "display: block;";
        commentList.style = "display: none;";
    }
    else {
        commentList.style = "display: block;";
        menuList.style = "display: none;";
    }
}

// does element contain classname
function containsClass(element, className) {
    return element.className.indexOf(className) > -1;
}

// is gui element selected
function isSelected(element) {
    return containsClass(element, "selected");
}

// get geolocation coordinates
function getLocation() {
    locationInput.value = "";
    searchError.text = "";
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        searchError.text = "Brskalnik ne podpira te funkcije.";
    }
}

// use geolocation to update map
function showPosition(position) {
    x = position.coords.latitude;
    y = position.coords.longitude;
    locationInput.value = x.toFixed(6) + "," + y.toFixed(6);
    if (typeof google !== 'undefined') {
        updateMap({lat: x, lng: y}, false);
    }
}

// error handler for geolocation
function showError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            searchError.text = "Dostop do lokacije zavrnjen s strani uporabnika."
            break;
        case error.POSITION_UNAVAILABLE:
            searchError.text = "Lokacija ni na voljo."
            break;
        case error.TIMEOUT:
            searchError.text = "Lokacija se ne odziva."
            break;
        case error.UNKNOWN_ERROR:
            searchError.text = "Neznana napaka pri dostopu do lokacije."
            break;
    }
}

// restaurant search. Create seardhID, attach it to url and redirect.
function search() {
    var searchID = "/search?filter=";
    //filters
    var filterButtons = document.getElementById("filters").getElementsByClassName("btn");
    for (var i = 0; i < filterButtons.length; i++) {
        searchID = searchID.concat(isSelected(filterButtons[i]) ? "t" : "f");
    }

    //criteria
    if (isSelected(criteria1)) {
        searchID = searchID.concat("&criteria=1&loc=");
    } else if (isSelected(criteria2)) {
        searchID = searchID.concat("&criteria=2&loc=");
    } else {
        searchError.text = "Izberite kriterij sortiranja.";
        return;
    }

    //location
    if (locationInput.value) {
        searchID = searchID.concat(locationInput.value)
        window.location.href =  searchID;
    } else {
        searchError.text = "Vnesite lokacijo.";
    }
}


// search gui handler. radio buttons and checkboxes
function updateSearchSettings(button, buttonGroup) {

    //checkbox type filters
    if (buttonGroup == "filters") {
        if (isSelected(button)) {
            button.className = button.className.replace(" selected", "");
        } else {
            button.className += " selected";
        }
    }
    else {
        //radiobutton type filters
        var buttons = document.getElementById(buttonGroup).getElementsByClassName("selected");
        for (var i = 0; i < buttons.length; i++) {
            buttons[i].className = buttons[i].className.replace(" selected", "");
            //console.log(buttons[i].className.replace(" selected", ""));
        }
        button.className += " selected";
    }
}

// initialize map
function initMap() {
    var home = {lat: 46.9580037, lng: 14.1080195};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: home
    });
    var marker = new google.maps.Marker({
        position: home,
        map: map
    });
}

//update google map. center to location, if search show results
function updateMap(coords, search) {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 11,
        center: coords
    });
    var bounds = new google.maps.LatLngBounds();

    var marker = new google.maps.Marker({
        position: coords,
        map: map,
        title: "vaša lokacija"
    });
    bounds.extend(marker.getPosition());

    if (search) {
        locations = document.getElementsByClassName("coordinates");
        for (var i = 0; i < locations.length; i++) {
            coords = locations[i].innerHTML.split(",");
            marker = new google.maps.Marker({
                position: {lat: parseFloat(coords[0]), lng: parseFloat(coords[1])},
                map: map,
                label: "" + (i + 1),
                title: locations[i].previousElementSibling.textContent,

            });
            bounds.extend(marker.getPosition());
        }
        map.fitBounds(bounds);
    }
}




