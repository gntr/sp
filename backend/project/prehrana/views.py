from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
import math
import datetime
from prehrana.forms import LoginForm, RegistrationForm, FeedbackForm, MenuForm, SearchForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import views
from prehrana.models import Restaurant, Feedback, Menu
from django.contrib.auth.models import User
import logging

log = logging.getLogger("prehrana")

# Create your views here.

app_name = 'prehrana'
from django.views.decorators.csrf import csrf_exempt

def login_view(request):
    """handle the login and registration requests"""
    error_view = views.login(request, template_name="prehrana/login.html",
                             extra_context={"message": "vneseni podatki niso veljavni"})

    if request.method == "POST":
        if request.POST.get('new_username', False):
            # registration form was submited
            form = RegistrationForm(request.POST)
            if form.is_valid():
                user = User.objects.create_user(username=form.cleaned_data['new_username'],
                                                password=form.cleaned_data['password'])
            else:
                return error_view
        else:
            # login form was submited
            form = LoginForm(request.POST)
            if form.is_valid():
                user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            else:
                return error_view
        if user is not None:
            # request was OK
            #login(request, user)
            return HttpResponseRedirect("/index/")
        else:
            return error_view
    return views.login(request, template_name="prehrana/login.html")


def index(request):
    return render(request, "prehrana/index.html", {"restaurant_list": Restaurant.objects.all().
                  order_by("-rating")[:5]})


def search(request):
    """handles search requests"""
    form = SearchForm(request.GET)
    if form.is_valid():
        return search_nearest(request)
    return render(request, "prehrana/search.html", {"restaurant_list": Restaurant.objects.all().
                  order_by("-rating")[:5]})


def search_nearest(request):
    """handles search for nearest restaurants"""
    filter_names = ["vegi", "lunch", "pizza", "sallad", "delivery", "fastfood", "invalid_friendly"]
    filters = {n: True for n, x in zip(filter_names, list(request.GET.get('filter', ''))) if x == "t"}
    criteria = request.GET.get('criteria', '')
    location = request.GET.get('loc', '').split(",")
    x = float(location[0])
    y = float(location[1])
    distances = [(math.sqrt((x - float(rest.longitude)) ** 2 + (y - float(rest.latitude)) ** 2), rest)
                 for rest in Restaurant.objects.filter(**filters)]
    restaurants = sorted([rest for _, rest in sorted(distances)[:5]],
                         key=(lambda x: x.price) if criteria == "1" else (lambda x: -x.rating))
    return render(request, "prehrana/search.html", {"restaurant_list": restaurants})


def locals(request, restaurant_id=1):
    """handles the locals view and feedback or menu POST requests"""
    if request.method == "POST" and request.user.is_authenticated:
        if request.POST.get('food', False):
            # comment was submited
            form_data = request.POST.dict()
            form_data["restaurant_id"] = restaurant_id
            form_data["user_id"] = request.user.id
            form = FeedbackForm(form_data)
            if form.is_valid():
                log.debug("user {} submited a feedback for restaurant {}: [rating={}, comment={}]".
                          format(request.user.id, restaurant_id, form_data["rating"], form_data["comment"]))
                form.save()
        elif request.user.has_perm("prehrana.edit_menu"):
            # menu was submited:
            form_data = request.POST.dict()
            form_data["restaurant_id"] = restaurant_id
            form = MenuForm(form_data)
            if form.is_valid():
                log.debug("user {} submited a menu for restaurant {}: [food={}]"
                          .format(request.user.id, restaurant_id, form_data["food"]))
                form.save()

    if request.GET.get('localName', False):
        restaurant_id = Restaurant.objects.get(name=request.GET.get('localName', '')).id

    context = {}
    context["restaurant_names"] = [r.name for r in Restaurant.objects.all()]
    context["restaurant"] = Restaurant.objects.get(pk=restaurant_id)
    context["feedbacks"] = Feedback.objects.filter(restaurant_id=restaurant_id)
    context["menus"] = Menu.objects.filter(restaurant_id=restaurant_id, date=datetime.date.today())
    return render(request, "prehrana/locals.html", context)


def contact(request):
    """contact page"""
    return render(request, "prehrana/contact.html")


def logout_user(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))
