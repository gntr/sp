from django import forms
import re
from prehrana.models import *




class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=50)
    password = forms.CharField(max_length=50, widget=forms.PasswordInput)


class RegistrationForm(forms.Form):
    new_username = forms.CharField(label='Username', max_length=50)
    password = forms.CharField(max_length=50, widget=forms.PasswordInput)
    password_repeated = forms.CharField(max_length=50, widget=forms.PasswordInput)


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ["user_id", "rating", "comment", "restaurant_id"]


class MenuForm(forms.ModelForm):
    class Meta:
        model = Menu
        fields = ["date", "food", "restaurant_id"]


class SearchForm(forms.Form):
    filter = forms.CharField(min_length=7)
    criteria = forms.CharField(min_length=1)
    loc = forms.CharField()

    def is_valid(self):
        if super(SearchForm, self).is_valid():
            if self.cleaned_data['filter'].strip("tf") == "":
                if self.cleaned_data['criteria'] in ["1", "2"]:
                    if len(re.findall(r"(-?\d{1,2}\.\d{6}),(-?\d{1,2}\.\d{6}$)", self.cleaned_data['loc']))==1:
                        return True
        return False


