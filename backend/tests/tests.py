
from django.test import TestCase
from.forms import SearchForm
from .models import Restaurant, Menu, Feedback, User


class SearchTest(TestCase):
    def test_is_valid(self):
        """
        is_valid() should return True, if the input dict is valid; False
        otherwise.
        """
        valid = SearchForm({"filter": "fftfftf", "criteria":"1", "loc":"12.123456,12.123456"})

        invalids = [SearchForm({"filter": "ffafftf", "criteria": "1", "loc": "12.123456,12.123456"}),
                    SearchForm({"filter": "ffffft", "criteria": "1", "loc": "12.123456,12.123456"}),

                    SearchForm({"filter": "fftfftf", "criteria": "3", "loc": "12.123456,12.123456"}),
                    SearchForm({"filter": "fftfftf", "criteria": "", "loc": "12.123456,12.123456"}),

                    SearchForm({"filter": "fftfftf", "criteria": "1", "loc": "12.12456,12.123456"}),
                    SearchForm({"filter": "fftfftf", "criteria": "1", "loc": "12.123456.12.123456"}),
                    SearchForm({"filter": "fftfftf", "criteria": "1", "loc": "12.123456,12.1234564"})]

        self.assertIs(valid.is_valid(), True)
        self.assertListEqual([x.is_valid() for x in invalids], [False for i in range(len(invalids))])


class TriggerTest(TestCase):
    """
    rating should be updated when new feedback is created.
    """
    def setUp(self):
        Restaurant.objects.create(name="a", address="a")
        User.objects.create(username='u')
        User.objects.create(username='v')
        Feedback.objects.create(rating=5, comment="a", restaurant_id=Restaurant.objects.get(name="a"),
                                user_id = User.objects.get(username='u'))
        Feedback.objects.create(rating=0, comment="a", restaurant_id=Restaurant.objects.get(name="a"),
                                user_id=User.objects.get(username='v'))

    def test_trigger(self):
        r = Restaurant.objects.get(name="a")
        self.assertEqual(r.rating, 2.5)
